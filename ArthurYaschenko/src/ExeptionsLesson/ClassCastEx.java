package ExeptionsLesson;

import java.util.ArrayList;
import java.util.Iterator;

public class ClassCastEx {
    public static void main(String[] args) {
        new ClassCastEx().castEx();

    }

    void castEx() {
        try {
            ArrayList list = new ArrayList();
            list.add("one");


            Iterator y = list.iterator();
            while (y.hasNext()) {
                Integer i = (Integer) y.next();

            }
        }
        catch (ClassCastException e){
            e.printStackTrace();
    }


    }
}
