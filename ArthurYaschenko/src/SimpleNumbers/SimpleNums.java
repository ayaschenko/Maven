package SimpleNumbers;

public class SimpleNums {
    public static void main(String[] args) {
        long currentNumber, dividers;
        for (currentNumber = 1;currentNumber<100000000; currentNumber++) {
            dividers = 0;
            for (int i = 1; i <= currentNumber; i++) {
                if (currentNumber % i == 0)
                    dividers++;
            }
            if (dividers <= 2)
                System.out.println(currentNumber);
        }
    }
}