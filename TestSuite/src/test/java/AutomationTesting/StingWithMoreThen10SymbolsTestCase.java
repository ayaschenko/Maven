package AutomationTesting;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.util.Strings;

public class StingWithMoreThen10SymbolsTestCase {
    @DataProvider(name = "TestStrings")
    public static Object[][] getData(){
        return new Object[][]{{"test", "test123456776", "testtest"}};

    }

    @Test(dataProvider = "TestStrings")
    public void stringsMethod (String testdata) throws Exception{
        try {
            Assert.assertTrue(testdata.length()<10);

        }
        catch (AssertionError e) {
            e.printStackTrace();
        }
    }

}
